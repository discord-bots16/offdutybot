import discord
import configparser
from discord.ext import commands
from discord.errors import Forbidden

# Set up Config
config = configparser.ConfigParser()
config.read('sierra.ini')
BOT_VERSION = config.get('Bot', 'Version')
BOT_OWNER = config.get('Bot', 'Owner')
BOT_REPO = config.get('Bot', 'Repo')
DISCORD_COLOR = int(config.get('Discord', 'Color'), 16)
DISCORD_PREFIX = config.get('Discord', 'Prefix')

# Custom help command to replace the default one

async def send_embed(ctx, embed):
    """
    Function that handles the sending of embeds
    - Takes context and embed to send
    - tries to send embed in channel
    - tries to send normal message when that fails
    - tries to send embed private with information about missing permissions
    """
    try:
        await ctx.send(embed=embed)
    except Forbidden:
        try:
            await ctx.send("Hey, seems like I can't send embeds. Please check my permissions :)")
        except Forbidden:
            await ctx.author.send(
                f"Hey, seems like I can't send any messages in {ctx.channel.name} on {ctx.guild.name}\n"
                f"You should probably let the server staff know about this. :slight_smile: ", embed=embed)


class Help(commands.Cog):#, description='Shows this help menu.'):
    # Sends this help message

    def __init__(self, bot):
        self.bot = bot

    @commands.command(name='help',
                      usage='help',
                      help='Shows this help menu!',
                      brief='Shows this help menu!')
    # @commands.bot_has_permissions(add_reactions=True,embed_links=True)
    async def help(self, ctx, *input):
        # Shows all modules of the bot

        # If input is provided, return help info for that specific Cog
        # Otherwise, return the main help dialog
        if not input:
            # Create embed
            helpMessage = discord.Embed(title='Commands and Modules', color=DISCORD_COLOR,
                                description=f'Use `{DISCORD_PREFIX}help <module>` for more detailed information '
                                            f'about that module.\n')

            # Get commands for each cog (Note: if the bot's scope balloons, this will need to be changed.
            for cog in self.bot.cogs:
                cog_desc = ''
                for command in self.bot.get_cog(cog).get_commands():
                    cog_desc += f'`{command.usage}` - {command.brief}\n'
                helpMessage.add_field(name=cog, value=cog_desc, inline=False)

            # Get uncategorized commands (commands where cog name is None and command isn't hidden)
            commands_desc = ''
            for command in self.bot.walk_commands():
                if not command.cog_name and not command.hidden:
                    commands_desc += f'`{command.usage}` - {command.brief}\n'
            if commands_desc:
                helpMessage.add_field(name='General Commands', value=commands_desc, inline=False)

            # Set author information
            helpMessage.add_field(name="About", value=f"I'm developed by Cliff#4877.\n\
                                    This specific version is maintained by {BOT_OWNER}\n\
                                    Please visit {BOT_REPO} to submit ideas or report bugs.")
            helpMessage.set_footer(text=f"I\'m currently running version {BOT_VERSION}")

        # When one Cog is provided
        elif len(input) == 1:

            # Find Cog
            for cog in self.bot.cogs:
                if cog.lower() == input[0].lower():
                    # Create embed
                    helpMessage = discord.Embed(title=f'{cog} Commands', description=self.bot.cogs[cog].description,
                                                color=DISCORD_COLOR)

                    # Get commands in the cog, if it's not hidden
                    for command in self.bot.get_cog(cog).get_commands():
                        if not command.hidden:
                            helpMessage.add_field(name=f"`{DISCORD_PREFIX}{command.name}`", value=command.help,
                                                  inline=False)
                    # Break when embed is complete
                    break

            # If we don't find a cog with that name
            else:
                helpMessage = discord.Embed(title="What's that?!",
                                    description=f"I can't seem to find anything named {input[0]}. "
                                                f"Are you sure that's right?",
                                    color=DISCORD_COLOR)

        # too many cogs requested - only one at a time allowed
        elif len(input) > 1:
            helpMessage = discord.Embed(title="One At A Time!",
                                description="Sorry, I can only do so much at once. Try just one module?",
                                color=DISCORD_COLOR)

        else:
            helpMessage = discord.Embed(title="I'm confused",
                                description="I uh... Don't know how you got here?\n"
                                            "I didn't see this coming at all.\n"
                                            "Would you please be so kind to report that issue to me in the repo?\n"
                                            f"{BOT_REPO}\n"
                                            "Thank you!",
                                color=DISCORD_COLOR)

        # Send whatever embed we decided on
        await send_embed(ctx, helpMessage)

    @commands.command(name='adminTutorial',
                      usage='adminTutorial',
                      brief='Shows a brief walkthrough for the Admin!',
                      help='Shows a brief walkthrough to tell the Admin how to set me up before users can start going '
                           'off-duty.')
    async def ShowAdminTutorial(self, ctx):
        # Outline initial setup process
        # Outline off-duty process
        # Outline on-duty process
        tutorialEmbed = discord.Embed(title='Admin Tutorial',
                                      color=DISCORD_COLOR,
                                      description=f'Hi there! I\'m a very minimal bot, whose sole purpose is to let '
                                                  f'your server staff take breaks without needing to feel like they\'re '
                                                  f'bothering your admin staff. Unfortunately, I don\'t work for your '
                                                  f'Server Owner, but as long as my role is the highest on the list, '
                                                  f'I should be able to set everyone else on the server off-duty.')
        tutorialEmbed.add_field(name='Step 1',
                                value='Make sure my role is above every role I should be able to manage.',
                                inline=False)
        tutorialEmbed.add_field(name='Step 2',
                                value=f'Use `{DISCORD_PREFIX}addRole` and `{DISCORD_PREFIX}setOffDuty` to tell me what '
                                      f'your leadership roles are, as well as which role I should give a user to indicate '
                                      f'that they\'re off-duty.',
                                inline=False)
        tutorialEmbed.add_field(name='Step 3',
                                value=f'I\'m now ready for users to use `{DISCORD_PREFIX}off-duty` and '
                                      f'`{DISCORD_PREFIX}on-duty` to toggle their on-duty status!',
                                inline=False)
        tutorialEmbed.add_field(name='Additional Notes',
                                value=f'`addRole` and `setOffDuty` are restricted to Admins on your server only. There '
                                      f'are a couple other Admin only commands as well that you shouldn\'t need often, '
                                      f'but you should know about anyways.\n'
                                      f'`remove <user>` will purge that user from the database. This is useful if you '
                                      f'need to remove a user from leadership while they are off-duty.\n'
                                      f'`removeRole <roles>` will remove the given roles from the list of roles I will '
                                      f'check when setting people on- and off-duty. Useful if you restructure your '
                                      f'leadership roles.\n'
                                      f'`purgeServer` is dangerous. It will completely remove *every* record I have of '
                                      f'this server, roles and users alike. Only use it when you\'re removing me from '
                                      f'your server, otherwise you will need to set this server up again from scratch.',
                                inline=False)

        await send_embed(ctx, tutorialEmbed)

    @commands.command(name='tutorial',
                      usage='tutorial',
                      brief='Shows a brief walkthrough!',
                      help='Shows a brief walkthrough of my main functionality!')
    async def ShowTutorial(self, ctx):
        # Outline initial setup process
        # Outline off-duty process
        # Outline on-duty process
        tutorialEmbed = discord.Embed(title='Tutorial',
                                      color=DISCORD_COLOR,
                                      description='',)
        tutorialEmbed.add_field(name='Going Off-Duty',
                                value=f'Use `{DISCORD_PREFIX}off-duty` to, well, go off-duty! I\'ll take off all your '
                                      f'leadership roles, and add your server\'s off-duty role to you. I\'ll make a note '
                                      f'of all your roles and when you went off-duty.',
                                inline=False)
        tutorialEmbed.add_field(name='Going Back On-Duty',
                                value=f'Use `{DISCORD_PREFIX}on-duty` to come back on-duty! If I have a note of you going '
                                      f'off-duty, I\'ll add those roles back to you, take off the off-duty role, and '
                                      f'erase that note.',
                                inline=False)
        await send_embed(ctx, tutorialEmbed)


def setup(bot):
    bot.add_cog(Help(bot))